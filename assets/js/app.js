// app.js
//require('../css/app.css');
require('../css/global.scss');
require('./dark-mode-switch');
require('popper.js');
require('bootstrap');
require('jquery');

$(document).ready(function(){
    $("#mode").click(function(){
        console.log("hello")
    });
});

$(function() {
    // bind 'myForm' and provide a simple callback function
    $('#delete').submit(function (event) {
        event.preventDefault(); //prevent default action
        $.ajax({
            url: $(this).attr("action"),
            type: $(this).attr("method"),
            data: new FormData(this),
            contentType: false,
            processData: false,
            success: function () { //
                //$("#server-results").html(response);
                alert("Operation success")
            },
            error: function (xhr) {
                alert("Impossible de pousuivre l'action, revoir vos paramètes : " + xhr.status + xhr.message)
            }
        })
    })
})