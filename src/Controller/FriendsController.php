<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FriendsController extends AbstractController
{
    /**
     * @Route("/addfriend", name="add_friend")
     * @param Request $request
     * @return Response|RedirectResponse
     */
    public function addFriend(Request $request)
    {
        $email = $request->request->get('email');
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findByEmail((string)$email);

        if (!$user)
        {
            return new Response('not found',Response::HTTP_NOT_FOUND);
        }
        $this->getUser()->addFriend($user);
        $em->flush();
        return $this->redirectToRoute('friends');

    }

    /**
     * @Route("/friends", name="friends")
     */
    public function index()
    {
        $user_friends = $this->getUser()->getFriends();
        return $this->render('friends/index.html.twig', [
            'friends' => $user_friends,
        ]);
    }
}
