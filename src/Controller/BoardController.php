<?php

namespace App\Controller;

use App\Entity\Board;

use App\Entity\BoardItems;
use App\Entity\User;
use App\Form\BoardItemsFormType;
use App\Form\BoardType;
use Doctrine\DBAL\Types\TextType;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Exception\AlreadySubmittedException;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
/*{
    "title":"Board api 88",
    "creation_date":"2020-04-21T18:25:43-05:00"
}
{
    "email":"hadramet.sylla@hadramet.fr",
    "username":"Hadramet2",
    "password":"Supinfo0!"
}
*/

class BoardController extends AbstractController
{
    /**
     * @Route("/user/boardItemsDrop/{id}", name="delete_all_items", methods={"POST"})
     * @param Board $board
     * @param $id
     * @return RedirectResponse
     */
    public function deleteAllItems(Board $board,$id)
    {
        $items = $board->getBoardItems();
        $em = $this->getDoctrine()->getManager();

        foreach ($items as $i)
        {
            $em->remove($i);
        }
        $em->flush();
        return $this->redirectToRoute('boards_view',['id'=>$id]);

    }

    /**
     * @Route("/user/board/{id}", name="boards_view")
     * @param Board $board
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function boardView(Board $board,Request $request,$id)
    {
        try
        {
            // Get the current user and board user
            $user = $this->getUser();
            $boarduser = $board->getUserId();

            // Form for board items
            try
            {

                $new_item = new BoardItems();
                $form = $this->createForm(BoardItemsFormType::class,$new_item);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid())
                {
                    $new_item = $form->getData();
                    $new_item->setBoard($board);
                    $new_item->addAssigneTo($this->getUser());
                    $new_item->setState("TODO");

                    $this->getDoctrine()->getManager()->persist($new_item);
                    $this->getDoctrine()->getManager()->flush();

                    return $this->redirectToRoute('boards_view',['id'=>$id]);
                }
            }catch (AlreadySubmittedException $e)
            {
                return $this->redirectToRoute('boards_view',['id'=>$id]);
            }

            // show the board it is user propertie
            if($boarduser != $user)
            { throw new NotFoundHttpException('Not Found');}
            $items = $board->getBoardItems();
            return $this->render('Page/boardView/boardView.html.twig', [
                "board_id" =>$board->getId(),
                "board_name" => $board->getTitle(),
                "items" => $items,
                "form" => $form->createView(),
                "error" => null
            ]);

        }catch (NotFoundHttpException $message)
        {
            return $this->render('Page/boardView/boardView.html.twig', [
                'error' => "Not Found In your repository",
                "board_name" => null,
                "form" => $form->createView(),
                "items" => null
            ]);
        }


    }


    /**
     * @Route("/api/boards", name="api_create_board", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function createBoard(Request $request)
    {
        // Get JsonContent
        $jsonContent = $request->getContent();

        // Deserialize
        $board = $this->get('serializer')
            ->deserialize($jsonContent,Board::class,'json');

        // Add board to user
        $this->getUser()->addBoard($board);

        // Save to Database
        $em = $this->getDoctrine()->getManager();
        $em->persist($board);
        $em->persist($this->getUser());

        $em->flush();

        return new Response('Success',Response::HTTP_CREATED);
    }

    /**
     * @Route("/api/boards/{id}", name="api_get_board_by_id",methods={"GET"},requirements={"id"="\d+"})
     * @param Board $board
     * @return mixed
     */
    public function showBoardById(Board $board)
    {
        try
        {
            // Get the current user and board user
            $user = $this->getUser();
            $boarduser = $board->getUserId();

            // show the board it is user propertie
            if($boarduser == $user)
            {
                //serialization
                $serializer = SerializerBuilder::create()->build();
                $data = $serializer->serialize($board,'json',
                    SerializationContext::create()->setGroups(array('detail')));

                // Response
                $response = new Response($data);
                $response->headers->set('Content-Type','application/json');

                return $response;

            }
            else
                {
                    throw $this->createNotFoundException(
                        'No Board found for id '
                    );
                }


        }catch (NotFoundHttpException $e)
        {
            return new Response("Not found ", Response::HTTP_NOT_FOUND);
        }

    }

    /**
     *@Route("/api/boards" ,name="api_list_board",methods={"GET"})
     */
    public function listBoard()
    {
        try
        {
            //Get User Boards
            $boards = $this->getUser()->getBoards();

            // If empty throw not found exception
            if (!$boards){
                throw $this->createNotFoundException(
                    'No Board found for id '
                );
            }

            // Serialization process
            $serializer = SerializerBuilder::create()->build();
            $jsonContent = $serializer->serialize($boards, 'json',
                SerializationContext::create()->setGroups(array('list','detail')));

            // Response
            $response = new Response($jsonContent);
            $response->headers->set('Content-Type','application/json');
            return $response;

        }catch ( NotFoundHttpException $e )
        {
            return new JsonResponse([],Response::HTTP_NOT_FOUND);
        }

    }

    /**
     * @Route("/api/boards/{id}",name="api_delete_board", methods={"DELETE"}, requirements={"id"="\d+"})
     * @param Board $board
     * @return Response
     */

    public function deleteBoard(Board $board)
    {
        try
        {
            // Get the current user and board user
            $user = $this->getUser();
            $boarduser = $board->getUserId();

            // if its user property
            if($boarduser == $user)
            {
                $user->removeBoard($board);

                // Remove from Database
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                return new Response('Delete success',Response::HTTP_OK);
            }else
            {
                throw $this->createNotFoundException(
                    'No Board found for id '
                );
            }


        }catch (NotFoundHttpException $e)
        {
            return new Response('Not found', Response::HTTP_NOT_FOUND);
        }

    }

    /**
     * @Route("/boards/{id}",name="delete_board", methods={"POST"}, requirements={"id"="\d+"})
     * @param Board $board
     * @return Response
     */
    public function deleteBoardM(Board $board)
    {
        try
        {
            $items = $board->getBoardItems();
            // Remove from Database
            $em = $this->getDoctrine()->getManager();

            foreach ($items as $i)
            {
                if ($i != null )  $board->removeBoardItem($i);
            }
            //$em->persist($board);
            $em->remove($board);
            $em->flush();

            return $this->redirectToRoute('home');

        }catch (NotFoundHttpException $e)
        {
            return $this->redirectToRoute('home');
        }

    }

    /**
     * @Route("/api/boards/{id}",name="api_update_board", methods={"PUT"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param $id
     * @return Response
     */

    public function updateBoard(Request $request,$id)
    {
        try
        {
            // Get the current user and data
            $user = $this->getUser();
            $data = $request->getContent();

            // Deserialize
            $board = $this->get('serializer')
                ->deserialize($data,Board::class,'json');

            // find the board
            $em = $this->getDoctrine()->getManager();
            $repo = $em->getRepository(Board::class);
            $old= $repo->findByIdAndUser($user,(int)$id);


            //if Not found error
            if (!$old)
            {
                throw $this->createNotFoundException(
                    'No Board found for id '
                );
            }

            //Update and Save
            $old[0]->setTitle($board->getTitle());
            $old[0]->setCreationDate($board->getCreationDate());
            $em->flush($old);

            return new Response('Update', Response::HTTP_OK);

        }catch (NotFoundHttpException $e)
        {
            return new Response('Not found', Response::HTTP_NOT_FOUND);

        }

    }

    // .../boards/{id}/items GET POST
    // .../boards/{id}/items/{id} GET PUT DELETE



}
