<?php

namespace App\Controller;

use App\Entity\Board;
use App\Form\BoardType;
use App\Security\UserLoginAuthenticator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use phpDocumentor\Reflection\Types\Array_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Exception\AlreadySubmittedException;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class DefaultController extends AbstractController
{

    /**
     * @Route("/user/home", name="home")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function index(Request $request) : Response
    {
        $boards_ = $this->getUser()->getBoards();
        $boards_items = array();

        foreach ($boards_ as $b)
        {
            foreach ($b->getBoardItems() as $i )
            {
                array_push($boards_items,$i);
            }
        }

        try
        {

            $board = new Board();
            $form = $this->createForm(BoardType::class,$board);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid())
            {
                $board = $form->getData();
                $board->setCreationDate(new \DateTime('now'));
                $board->setUserId($this->getUser());

                $this->getDoctrine()->getManager()->persist($board);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('home');
            }
        }catch (AlreadySubmittedException $e)
        {
            return $this->redirectToRoute('home');
        }


        return $this->render('default/index.html.twig', [
            'boards'=> $boards_,
            'items' => $boards_items,
            'friends' => $this->getUser()->getFriends(),
            'form' => $form->createView()
        ]);
    }
}

