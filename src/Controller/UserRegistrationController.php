<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserRegistrationController extends AbstractController
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    /**
     * @Route("/api/register", name="api_user_register", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */

    public function registerAction(Request $request)
    {
        $user = new User();
        $data = $request->getContent();

        $jsonContent = $this->get('serializer')
            ->deserialize($data,User::class,'json');

        //encode password
        $password = $this->encoder->encodePassword($jsonContent,$jsonContent->getPassword());

        // Generate Token
        $token = bin2hex(openssl_random_pseudo_bytes(16));

        //set value
        $user->setPassword($password);
        $user->setApiToken($token);
        $user->setRoles(['ROLE_USER']);
        $user->setEmail($jsonContent->getEmail());
        $user->setUsername($jsonContent->getUsername2());


        // Save to database
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        // Response
        return new JsonResponse(["message" => "Success"],Response::HTTP_CREATED) ;
    }
}
//var_dump($user);die();