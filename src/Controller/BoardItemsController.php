<?php

namespace App\Controller;

use App\Entity\Board;
use App\Entity\BoardItems;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
/*{
    "name":"Board items 7",
    "state": "TODO",
    "due_date": "2020-04-21T18:25:43-05:00",
    "description": "This is a description for boards items uno, number one, first, singué, ein, ich"
} */
class BoardItemsController extends AbstractController
{
    /**
     * @Route("/assigneto/{id}/{user_id}", name="assigne_to")
     * @param BoardItems $items
     * @param $user_id
     * @return RedirectResponse
     */
    public function assigneTo(BoardItems $items, $user_id)
    {
        $friend ='';
        foreach ($this->getUser()->getFriends() as $f)
        {
            if ($f->getId() == $user_id)
            {
                $friend = $f;
                $items->addAssigneTo($friend);
                $this->getDoctrine()->getManager()->flush();
            }
        }

        return $this->redirectToRoute('boards_view',['id'=>$items->getBoard()->getId()]);
    }

    /**
     * @Route("/boards/items/{id}/{state}",name="change_item_state", methods={"POST","GET"})
     * @param BoardItems $items
     * @param $state
     * @return RedirectResponse
     */
    public function changeState(BoardItems $items, $state)
    {
        $items->setState($state);
        $em = $this->getDoctrine()->getManager();
        $em->persist($items);
        $em->flush();
        return $this->redirectToRoute('boards_view',['id'=>$items->getBoard()->getId()]);
    }
    /**
     * @Route("/boards/items/{id}", name="delete_items", methods={"POST"},requirements={"id"="\d+"})
     * @param BoardItems $items
     * @return Response
     */
    public function deleteItems(BoardItems $items) {
        $user = $this->getUser();
        $user->removeBoardItem($items);


        // Remove from Database
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->remove($items);
        $em->flush();
        return $this->redirectToRoute('boards_view',['id'=>$items->getBoard()->getId()]);
    }

    /**
     * @Route("/api/boards/{id}/items", name="api_boards_items", methods={"POST"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param $id
     * @return JsonResponse|Response
     */
    public function createBoardsItems(Request $request,$id)
    {
        // Pre set
        $user = $this->getUser();
        $boards = $user->getBoards();
        $em = $this->getDoctrine()->getManager();


        // Get JsonContent
        $jsonContent = $request->getContent();

        // Deserialize
        $boardsItems = $this->get('serializer')
            ->deserialize($jsonContent,BoardItems::class,'json');

        try
        {
            if (!$boards)
            {
                throw $this->createNotFoundException(
                    'No Board found for id '
                );
            }
            // board init
            $board_ ="";
            foreach ($boards as $b )
            {
                if ($b->getId()== $id)
                {
                    $board_ = $b;
                }
            }
        }catch (NotFoundHttpException $e)
        {
            return new JsonResponse([],Response::HTTP_NOT_FOUND);
        }

        try
        {
            if (!$board_)
            {
                throw $this->createNotFoundException(
                    'No Board found for id '
                );
            }

            // Save to data base
            $board_->addBoardItem($boardsItems);
            $user->addBoard($board_);
            $user->addBoardItem($boardsItems);

            $em->persist($board_);
            $em->persist($boardsItems);
            $em->persist($user);

            $em->flush();

            return  new Response("Board Item create with success", Response::HTTP_OK);

        }catch (NotFoundHttpException $e)
        {
            return new JsonResponse([],Response::HTTP_NOT_FOUND);
        }

    }

    /**
     * @Route("/api/boards/{id}/items", name="api_boards_items_byboard", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function getBoardsItems($id)
    {
        // Find item
        $boads_= $this->getUser()->getBoards();
        $items_ = "";
        foreach ($boads_ as $b )
        {
            if ($b->getId() == $id)
            {
                $items_=$b->getBoardItems();
            }
        }

        // Serialization
        $serializer = SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($items_, 'json',
            SerializationContext::create()->setGroups(array('items')));

        // Response
        $response = new Response($jsonContent);
        $response->headers->set('Content-Type','application/json');
        return $response;
    }

    /**
     * @Route("/api/boards/items", name="api_user_items", methods={"GET"})
     */
    public function getUserItems()
    {
        // Find item
        $items= $this->getUser()->getBoardItems();

        // Serialization
        $serializer = SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($items, 'json',
            SerializationContext::create()->setGroups(array('items')));

        // Response
        $response = new Response($jsonContent);
        $response->headers->set('Content-Type','application/json');
        return $response;
    }
}


// /boards/{b_id}/items GET POST
// /boards/items/{id} GET PUT DELETE