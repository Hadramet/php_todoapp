<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups ;
/**
 * @ORM\Entity(repositoryClass="App\Repository\BoardItemsRepository")
 */
class BoardItems
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"items"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=10)
     * @Groups({"items"})
     */
    private $state;

    /**
     * @ORM\Column(type="date")
     * @Groups({"items"})
     */
    private $due_date;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Board", inversedBy="boardItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $board;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="boardItems")
     * @Groups({"detail"})
     */
    private $assigne_to;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    public function __construct()
    {
        $this->assigne_to = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getDueDate(): ?\DateTimeInterface
    {
        return $this->due_date;
    }

    public function setDueDate(\DateTimeInterface $due_date): self
    {
        $this->due_date = $due_date;

        return $this;
    }

    public function getBoard(): Board
    {
        return $this->board;
    }

    public function setBoard(Board $board): self
    {
        $this->board = $board;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getAssigneTo(): Collection
    {
        return $this->assigne_to;
    }

    public function addAssigneTo(User $assigneTo): self
    {
        if (!$this->assigne_to->contains($assigneTo)) {
            $this->assigne_to[] = $assigneTo;
        }

        return $this;
    }

    public function removeAssigneTo(User $assigneTo): self
    {
        if ($this->assigne_to->contains($assigneTo)) {
            $this->assigne_to->removeElement($assigneTo);
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
