<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups ;
/**
 * @ORM\Entity(repositoryClass="App\Repository\BoardRepository")
 */
class Board
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list","detail"})
     */
    private $title;

    /**
     * @ORM\Column(type="date")
     * @Groups({"list","detail"})
     */
    private $creation_date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="boards")
     * @Groups({"list"})
     *
     */
    private $user_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BoardItems", mappedBy="board")
     */
    private $boardItems;

    public function __construct()
    {
        $this->boardItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creation_date;
    }

    public function setCreationDate(\DateTimeInterface $creation_date): self
    {
        $this->creation_date = $creation_date;

        return $this;
    }

    public function getUserId(): User
    {
        return $this->user_id;
    }

    public function setUserId(User $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return Collection|BoardItems[]
     */
    public function getBoardItems(): Collection
    {
        return $this->boardItems;
    }

    public function addBoardItem(BoardItems $boardItem): self
    {
        if (!$this->boardItems->contains($boardItem)) {
            $this->boardItems[] = $boardItem;
            $boardItem->setBoard($this);
        }

        return $this;
    }

    public function removeBoardItem(BoardItems $boardItem): self
    {
        if ($this->boardItems->contains($boardItem)) {
            $this->boardItems->removeElement($boardItem);
            // set the owning side to null (unless already changed)
            if ($boardItem->getBoard() === $this) {
                $boardItem->setBoard(null);
            }
        }

        return $this;
    }
}
