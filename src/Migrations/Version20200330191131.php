<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200330191131 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE board_items (id INT AUTO_INCREMENT NOT NULL, board_id INT NOT NULL, name VARCHAR(255) NOT NULL, state VARCHAR(10) NOT NULL, due_date DATE NOT NULL, description VARCHAR(255) DEFAULT NULL, INDEX IDX_43A30E66E7EC5785 (board_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE board_items_user (board_items_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_2671AC73FB47AD3D (board_items_id), INDEX IDX_2671AC73A76ED395 (user_id), PRIMARY KEY(board_items_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE board_items ADD CONSTRAINT FK_43A30E66E7EC5785 FOREIGN KEY (board_id) REFERENCES board (id)');
        $this->addSql('ALTER TABLE board_items_user ADD CONSTRAINT FK_2671AC73FB47AD3D FOREIGN KEY (board_items_id) REFERENCES board_items (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE board_items_user ADD CONSTRAINT FK_2671AC73A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE board CHANGE user_id_id user_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE api_token api_token VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE board_items_user DROP FOREIGN KEY FK_2671AC73FB47AD3D');
        $this->addSql('DROP TABLE board_items');
        $this->addSql('DROP TABLE board_items_user');
        $this->addSql('ALTER TABLE board CHANGE user_id_id user_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE api_token api_token VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
