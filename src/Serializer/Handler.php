<?php


namespace App\Serializer\Handler;


use App\Entity\Board;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use JMS\Serializer\SerializationContext;

class BoardHandler implements SubscribingHandlerInterface
{
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => 'App\Entity\Board',
                'method' => 'serialize'
            ],
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format' => 'json',
                'type' => 'App\Entity\Board',
                'method' => 'deserialize',
            ]
        ];

    }

    public function serialize (JsonSerializationVisitor $visitor,
                               Board $board, array $type, SerializationContext $context)
    {
        $date = new \DateTime();
        $data=
            [
                'title' => $board->getTitle(),
                'created_at'=> $board->getCreationDate(),
                'serialized_at' => $date->format('l jS \of F Y h:i:s A')
            ];
        return $visitor->visitArray($data,$type,$context);
    }

}