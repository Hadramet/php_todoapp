<?php

namespace App\Repository;

use App\Entity\BoardItems;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BoardItems|null find($id, $lockMode = null, $lockVersion = null)
 * @method BoardItems|null findOneBy(array $criteria, array $orderBy = null)
 * @method BoardItems[]    findAll()
 * @method BoardItems[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoardItemsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BoardItems::class);
    }

    // /**
    //  * @return BoardItems[] Returns an array of BoardItems objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BoardItems
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
