# SUPINFO PHP Project (Symfony)
``by SYLLA Hadramet``


[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

### Tech
Nous avons utilisé [Symfony]() v4.0.3 dans ce projet, mais aussi :

* [JQuerry](https://jquery.com/) - Comme ressource javascript.
* [Bootstrap](https://getbootstrap.com/) - Pour le front.
* [WebPack]()
* [MySQL]()
* [PHP v7]()
* [NodeJS]()

Si vous n'avez pas le fichier du projet vous le trouverez sur mon repertoire public [GitLab](https://gitlab.com/Hadramet/php_todoapp).

### Installation
Ce projet nécessite [php 7](), [symfony]() ,[yarn](), [composer]() pour fonctionner, donc si vous ne les avez pas installer télécharger et installer les,
rapidement ça ne prend que 3 min.

Une fois l'installation terminé, verifier que tout fonctionne :
````shell script
$ symfony --version
$ php --version
$ composer --version
````

Installation des dépendances et modules javascript.
```shell script
$ cd php_project

$ composer install

$ yarn install /ou nmp install
```

### DataBase 
Avant le lancement du serveur, il faut configurer l'environement du project, pour la base de donnée :
se rendre dans le fichier .env et modifier la ligne suivante en fonction des paramètres de votre serveur mysql.
* USER_NAME : localhost ou un autre utilisateur
* USER_PASSWORLD : Mot de pass de l'utilisateur

et promis c'est la seule configuration manuelle à faire ! :)
````text
DATABASE_URL=mysql://USER_NAME:USER_PASSWORD0@127.0.0.1:3306/php_projet_database?serverVersion=4.3
````
ensuit nous allons créer la base de donnée. Pour cela nous avons un module Symfony appelé Doctrine qui soccupe de tout,
lancé les commandes suivante :

````shell script
$ php bin/console doctrine:database:create
````
qui va créer la base de donnée ``php_projet_database`` ensuite lancé les commandes suivantes pour créer les tables de la
base de données :
* Taper Entrer pour toutes questions.
````shell script
$ php bin/console make:migration
$ php bin/console doctrine:migrations:migrate
````
et voila si tout s'est bien passé, en verifiant votre server (phpmyadmin) vous verrez que la base de données à été créer
ainsi que toutes ses tables.

### Front-end Building
Il faut aussi configurer les styles et scripts se trouvant dans le repertoire /assets. Pour cela, vu que nous utilisons
webpack.config.js, lancer la commande suivante:
````shell script
$ yarn encore dev
````
Cette commande vas construire nos scripts [JavaScript]() et [SCSS]() dans un repertoire /public/build, ce sont ces fichiers
qui seront utilisé comme script dans les pages html du projet.

### Lancement du server
Enfin si vous n'avez sauté aucune étape, tout doit fonctionner correctement pour la suite. Utiliser la commande ci dessous pour 
lancer le server: 
````shell script
$ symfony server:start
````
Rendez-vous sur le lien affiché, allez sur la page de login, créer un compte en cliquant sur le lien en bas, ensuite vous serez
redirigé sur la page de l'application. Vous pourrez tester toutes les fonctionnalités qui vous sont proposées.

Merci !



